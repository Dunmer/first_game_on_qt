#include <QDateTime>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QTimer>

#include <math.h>
#include <stdlib.h>

#include "cannonfield.h"

CannonField::CannonField(QWidget *parent)
    : QWidget(parent)
{
    currentAngle = 45;
    currentForce = 0;
    count = 0;
    chetTimerCount = 0;
    timerCount = 0;
    autoShootTimer = new QTimer(this);
    connect(autoShootTimer, SIGNAL(timeout()), this, SLOT(moveShot()));
    cheatTimer = new QTimer(this);
    connect(cheatTimer, SIGNAL(timeout()), this, SLOT(moveTrajectoty()));
    shootAngle = 0;
    shootForce = 0;
    target = QPoint(0, 0);
    gameEnded = false;
    barrelPressed = false;
    setPalette(QPalette(QColor(250, 250, 200)));
    setAutoFillBackground(true);
    newTarget();
}

void CannonField::setAngle(int angle)
{
    if (angle < 5)
        angle = 5;
    if (angle > 70)
        angle = 70;
    if (currentAngle == angle)
        return;
    currentAngle = angle;
    update(cannonRect());
    emit angleChanged(currentAngle);
}

void CannonField::setForce(int force)
{
    if (force < 0)
        force = 0;
    if (currentForce == force)
        return;
    currentForce = force;
    update(cannonRect());
    emit forceChanged(currentForce);
}

void CannonField::shoot()
{
    if (isShooting())
        return;
    timerCount = 0;
    shootAngle = currentAngle;
    shootForce = currentForce;
    autoShootTimer->start(5);
    emit canShoot(false);
}

void CannonField::cheat()
{
    if (cheatTimer->isActive())
        return;
    chetTimerCount = 0;
    shootAngle = currentAngle;
    shootForce = currentForce;

    cheatTimer->start(5);
}

void CannonField::newTarget()
{
    static bool firstTime = true;

    if (firstTime) {
        firstTime = false;
        QTime midnight(0, 0, 0);
        qsrand(midnight.secsTo(QTime::currentTime()));
    }
    target = QPoint(200 + qrand() % 190, 10 + qrand() % 255);
    update();
}

void CannonField::setGameOver()
{
    if (gameEnded)
        return;
    if (isShooting())
        autoShootTimer->stop();
    gameEnded = true;
    update();
}

void CannonField::restartGame()
{
    if (isShooting())
        autoShootTimer->stop();
    gameEnded = false;
    update();
    emit canShoot(true);
}

void CannonField::moveShot()
{
    QRect region = shotRect().boundingRect();
    ++timerCount;

    QRect shotR = shotRect().boundingRect();

    if (shotR.intersects(targetRect())) {
        autoShootTimer->stop();
        emit hit();
        emit canShoot(true);
    } else if (shotR.x() > width() || shotR.y() > height()
               || shotR.intersects(barrierRect())) {
        autoShootTimer->stop();
        emit missed();
        emit canShoot(true);
    } else {
        region = region.united(shotR);
        const int margin = 16;
        region.adjust(-margin, -margin, margin, margin);

    }
    update(region);
    update(cannonRect());
}

void CannonField::moveTrajectoty()
{
    ++chetTimerCount;

    //Если прошло 5 секунд останавливаем таймер
    if( ( chetTimerCount * 5) >= 5000  )
    {
        cheatTimer->stop();
        count = 0;
    }

    update();
}

void CannonField::TrajectoryLine(QPainter &painter)
{
    const double gravity = 4;

    double time = chetTimerCount / 20.0;
    double backStepTime = chetTimerCount / 30.0;
    double velocity = shootForce;
    double radians = shootAngle * 3.14159265 / 180;

    double velx = velocity * cos(radians);
    double vely = velocity * sin(radians);
    //Костыль, НО работает
    double x0 = (34 + currentForce + 5) * cos(radians);
    double y0 = (34 + currentForce + 5) * sin(radians);

    //предидущая размерность по времени
    double previous_x = x0 + velx * backStepTime;
    double previous_y = y0 + vely * backStepTime - 0.5 * gravity * backStepTime * backStepTime;

    double x = x0 + velx * time;
    double y = y0 + vely * time - 0.5 * gravity * time * time;

    QPoint tmp( qRound(previous_x), height() - 1 - qRound(previous_y) );
    QPoint result(qRound(x), height() - 1 - qRound(y));

    a[count] = tmp;
    a[count + 1] = result;

    painter.setPen(QPen(Qt::red,2, Qt::DashLine));
    painter.drawPolyline(a,count+1);
    count++;

}

void CannonField::mousePressEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
        return;
    if (barrelHit(event->pos()))
        barrelPressed = true;
}

void CannonField::mouseMoveEvent(QMouseEvent *event)
{
    if (!barrelPressed)
        return;
    QPoint pos = event->pos();
    if (pos.x() <= 0)
        pos.setX(1);
    if (pos.y() >= height())
        pos.setY(height() - 1);
    double rad = atan(((double)rect().bottom() - pos.y()) / pos.x());
    setAngle(qRound(rad * 180 / 3.14159265));
}

void CannonField::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        barrelPressed = false;
}

void CannonField::paintEvent(QPaintEvent * /* event */)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    if (gameEnded) {
        painter.setPen(Qt::black);
        painter.setFont(QFont("Courier", 48, QFont::Bold));
        painter.drawText(rect(), Qt::AlignCenter, tr("Game Over"));
    }
    paintCannon(painter);
    paintBarrier(painter);
    if (isShooting())
        paintShot(painter);
    if (!gameEnded)
        paintTarget(painter);

    if (cheatTimer->isActive())
        TrajectoryLine(painter);
}

void CannonField::paintShot(QPainter &painter)
{
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawEllipse(shotRect().boundingRect());
}

void CannonField::paintTarget(QPainter &painter)
{
    painter.setPen(Qt::black);
    painter.setBrush(Qt::red);
    painter.drawRect(targetRect());
}

void CannonField::paintBarrier(QPainter &painter)
{
    painter.setPen(Qt::black);
    painter.setBrush(Qt::yellow);
    painter.drawRect(barrierRect());
}

const QRect barrelRect(30, -5, 20, 10);

void CannonField::paintCannon(QPainter &painter)
{
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::blue);

    painter.save();
    painter.translate(0, height());
    painter.drawPie(QRect(-35, -35, 70, 70), 0, 90 * 16);
    if (autoShootTimer->isActive())
        painter.setBrush(Qt::red);
    painter.rotate(-currentAngle);
    painter.drawRect(34, -5, currentForce, 10);
    painter.restore();
}

QRect CannonField::cannonRect() const
{
    QRect result(0, 0, 100, 100);
    result.moveBottomLeft(rect().bottomLeft());
    return result;
}

QRegion CannonField::shotRect() const
{
    const double gravity = 4;

    double time = timerCount / 20.0;
    double velocity = shootForce;
    double radians = shootAngle * 3.14159265 / 180;

    double velx = velocity * cos(radians);
    double vely = velocity * sin(radians);
    //Магическое число 34 означающие конец дуги пушки
    double x0 = (34 + currentForce + 5) * cos(radians);
    double y0 = (34 + currentForce + 5) * sin(radians);
    double x = x0 + velx * time;
    double y = y0 + vely * time - 0.5 * gravity * time * time;

    QRect result(0, 0, 9, 9);
    result.moveCenter(QPoint(qRound(x), height() - 1 - qRound(y)));
    return QRegion (result);
}

QRect CannonField::targetRect() const
{
    QRect result(0, 0, 20, 10);
    result.moveCenter(QPoint(target.x(), height() - 1 - target.y()));
    return result;
}

QRect CannonField::barrierRect() const
{
    return QRect(145, height() - 100, 15, 99);
}

bool CannonField::barrelHit(const QPoint &pos) const
{
    QMatrix matrix;
    matrix.translate(0, height());
    matrix.rotate(-currentAngle);
    matrix = matrix.inverted();
    return barrelRect.contains(matrix.map(pos));
}

bool CannonField::isShooting() const
{
    return autoShootTimer->isActive();
}

QSize CannonField::sizeHint() const
{
    return QSize(400, 300);
}
