#include "widget.h"
#include <QApplication>

#include "gameboard.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    GameBoard board;
    board.setGeometry(100, 100, 500, 355);
    board.show();

    return a.exec();
}
